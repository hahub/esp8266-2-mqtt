# README #

Welcome to "ESP8266 Station" project.

Hi, 

Are you also making your home "smart"? Or have some "crazy" IoT project in your head? 

I wanted to share my version of the ESP8266 station - to handle simple sensors' and actuators' combinations and bridge them to MQTT (and consume/control by OpenHAB).
As a result of couple weekends and evenings I have this quick prototype  (mainly because have to finish my "smart" irrigation system ASAP :) summer is short in Quebec ) and now sharing it with the community.
(and as a "side effect" i've got 4 stations already running in my house in order to get only one irrigation ...)

### The goal of this project is to build a reusable sensor/actuator platform ...  ###

- ...well, the idea is to minimize hardware programming and be able to build a "station" for any location with desired configuration of sensors/actuators consumable by OpenHAB
- I will add functionality as soon as I need it for my "stations" in various locations in my house.
- all the integrations with OpenHAB are done in a uniform way - through MQTT topics - receive commands(ON/OFF), send back states(ON/OFF) or decimal values(like 42.42). [no security for now , so be careful! good to add Authz and TLS on top of MQTT in the future]
- So the plan is to focus on OpenHab but not on C++ and hardware soldering..
- Current version is 0.2 . First stable after couple weeks actually running on 4 stations:
    * Kitchen station (DHT22, proximity sensor, luminosity sensor, PIR motion sensor, 1 relay board) - [not pushed yet]. It is used to turn on/off/maintain the lights over the counters. And yes, kitchen is a hot place - so DHT22 precision is needed :)
    * HVAC extension station (DHT11, DS18B20, vacuum sensor). I'm using it to measure actual air temperature going out of the HVAC turbine as well as vacuum sensor tells me if the turbine is actually pumping the air. It is very helpful to augment ZWAVE thermostat with data from the trenches :). Side DHT11 is used to measure temperature and humidity of the air in the basement (yes, to prevent molds :) and turn on dehumidifier..)
    * Office station - (DHT11, PIR motion) I use it to turn on/off the light when I enter or leave the office also it triggers some other events in OpenHAB.
    * Irrigation station - (ICS USB/UART 8-relay module to control sprinklers, Side DHT11 to measure basement inner wall conditions). Relays are mutually exclusive. trying to make it fool-proof...

The project will evolve as my "smart home" evolves. And I add here only what is really needed to minimize C++ and hardware -related efforts.

### How do I get set up? ###

* The board tested with the project: NODEMCU or LoLin. Any 12E should be good.
* IDE I'm using Arduino and Visual Studio on top of it (vMicro plugin)
* Configuration - you need to create WiFi.h with your network credentials ( #define them). MQTT server address. configure your topic names constants.
There is also Settings.h file with some additional config data.
* I recommend MQTT Spy to test it.
* Dependencies (... DHT sensor lib, DTSensor lib) - nothing special all in Lib manager.
* MQTT service - yes you need one. Mosquitto should be fine.

### Who do I talk to? ###

* drop me an email cdromka@gmail.com  if you have questions. Roman Martynenko
* Licensing - MIT - just use it if you like it...