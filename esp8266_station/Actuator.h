// Actuator.h

#ifndef _ACTUATOR_h
#define _ACTUATOR_h

#include <Arduino.h>
#include <PubSubClient.h>

class Actuator
{
public:
	Actuator();
	Actuator(int pin, int onState, int initialState, const char* topic, const char* stateTopic, long throttleDelay);
	virtual void subscribe(PubSubClient& client);
	virtual bool updateState(char * topic, unsigned char * payload, unsigned int length, PubSubClient& client);
	virtual bool updateState(uint pin, uint value);
	virtual void publishState(PubSubClient& client);
	virtual void pollState(PubSubClient & client, bool forced = false);
	virtual bool updateState(uint8_t newState);
	virtual bool overrideState(uint8_t newState, PubSubClient & client);
	inline bool isMutuallyExclusive() { return _mutuallyExclusive; }
protected:
	virtual void init(int pin, int onState, int initialState, const char* topic, const char* stateTopic, long throttleDelay);
	int _pin;
	int _onState;
	int _state;
	long _throttleDelay;
	long _lastUpdate = 0;
	char _topic[50] = {};
	char _stateTopic[50] = {};
	long _lastStatePublished = 0;
	long _pubStateInterval = 65432;	// publish states every minute (kinda :) )
	bool _mutuallyExclusive = false;	 // set to true to always go OFF when someone in my group goes ON
};

#endif

