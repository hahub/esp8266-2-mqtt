#include "Flags.h"
#include "Station.h"

// ToDo: rewrite mqtt global relay callback to use class method instead of global function!!!
Station* station_ref = NULL;
void mqtt_callback(char * topic, unsigned char * payload, unsigned int length)
{
	station_ref->updateActuatorsFromMqtt(topic, payload, length);
}

Station::Station(const char* wifiSSID, const char* wifiPassword,
	const char* mqttIP, int mqttPort, const char* mqttClient,
	const char* anouncementTopic)
{
	_wifiSSID = wifiSSID;
	_wifiPassword = wifiPassword;
	_mqttIP = mqttIP;
	_mqttPort = mqttPort;
	_mqttClient = mqttClient;
	_anouncementTopic = anouncementTopic;
	station_ref = this;
}

void Station::connectWiFi()
{
	delay(10);
	log("Connecting to ");
	log(_wifiSSID);

	WiFi.begin(_wifiSSID, _wifiPassword);

	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		log(".");
	}

	log("");
	log("WiFi connected");
	log("IP address: ");
	//	log(WiFi.localIP());
}

void Station::refresh()
{
	switch (_step) {
	case 1:
		// Loop until we're reconnected
		while (!_pubSubClient.connected()) {
			_pubSubClient.setClient(_espClient);
			if (WiFi.status() != WL_CONNECTED)
			{
				connectWiFi();
			}
			_pubSubClient.setServer(_mqttIP, _mqttPort);
			_pubSubClient.setCallback(mqtt_callback);
			log("Attempting MQTT connection...");
			if (_pubSubClient.connect(_mqttClient)) {
				log("connected");
				subscribeActuators();
				_pubSubClient.publish(_anouncementTopic, _mqttClient);
			}
			else {
				log("failed, rc=");
				//			log(_pubSubClient.state());
				log(" try again in 5 seconds");
				delay(5000);
			}
		}; _step = 2; break;
	case 2: refreshSensors();
		pollActuatorsState();
		_pubSubClient.loop();
		_step = 1; break;
	}
}

void Station::refreshSensors()
{
	if (_currentSensor < _sensorCount)
	{
		_sensors[_currentSensor++]->refresh(_pubSubClient);
	}
	if (_currentSensor >= _sensorCount)
		_currentSensor = 0;
}

void Station::registerSensor(Sensor* sensor)
{
	_sensors[_sensorCount++] = sensor;
}

void Station::updateActuatorsFromMqtt(char * topic, unsigned char * payload, unsigned int length)
{
	for (int i = 0; i < _actuatorCount; i++)
	{
		_actuators[i]->updateState(topic, payload, length, _pubSubClient);
	}
}

void Station::registerActuator(Actuator * actuator)
{
	_actuators[_actuatorCount++] = actuator;
}

// ToDo: find a good logging lib to keep serial free...
void Station::log(const char * msg)
{
#ifdef LOGGING_ON
	Serial.println(msg);
#endif
}

void Station::log(double val)
{
#ifdef LOGGING_ON
	Serial.println(val);
#endif
}

void Station::subscribeActuators()
{
	for (int i = 0; i < _actuatorCount; i++)
	{
		_actuators[i]->subscribe(_pubSubClient);
	}
}

void Station::pollActuatorsState()
{
	for (int i = 0; i < _actuatorCount; i++)
	{
		_actuators[i]->pollState(_pubSubClient);
	}
}



