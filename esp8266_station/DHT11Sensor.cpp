#include "DHT11Sensor.h"

DHT11Sensor::DHT11Sensor(int type, int pin, const char* mqttTopic, long throttleDelay)
{
	_pin = pin;
	_binary = false;
	_mqttTopic = mqttTopic;
	_throttleDelay = throttleDelay;
	_isTemperature = type == TEMPERATURE;
	_reportIfUnchanged = true;	// this is low precision slow sensor. so force it to report even if unchanged.
}

DHT11Sensor::DHT11Sensor(int type, int pin, DHT * indht, const char * mqttTopic, long throttleDelay)
{
	_pin = pin;
	_dht = indht == NULL ? new DHT(pin, DHT11) : indht;
	_binary = false;
	_mqttTopic = mqttTopic;
	_throttleDelay = throttleDelay;
	_isTemperature = type == TEMPERATURE;
	_lastPoll = millis();
	_reportIfUnchanged = true;	// this is low precision slow sensor. so force it to report even if unchanged.
}

float DHT11Sensor::getValue()
{
	byte t = 0, h = 0;
	float val = _isTemperature ? _dht->readTemperature() : _dht->readHumidity();
	return cleanupNoise(val);	//return old value if error
}
