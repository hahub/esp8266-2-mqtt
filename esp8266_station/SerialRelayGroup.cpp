#include "Flags.h"
#include "Station.h"
#include "SerialRelay.h"
#include "SerialRelayGroup.h"

// Utils. ToDo: move to a separate util class
void PrintHex8(uint8_t *data, uint8_t length) // prints 8-bit data in hex with leading zeroes
{
#ifdef LOGGING_ON
	Serial.print("0x");
	for (int i = 0; i < length; i++) {
		if (data[i] < 0x10) { Serial.print("0"); }
		Serial.print(data[i], HEX);
		Serial.print(" ");
	}
#endif
}

SerialRelayGroup::SerialRelayGroup() {}

SerialRelayGroup::SerialRelayGroup(int pin, int onState, int initialState, const char* topic, const char* stateTopic, long throttleDelay)
{
	init(pin, onState, initialState, topic, stateTopic, throttleDelay);
}

SerialRelayGroup::SerialRelayGroup(int pin, int onState, int initialState, const char* topic, const char* stateTopic, long throttleDelay, long pubStateInterval)
{
	init(pin, onState, initialState, topic, stateTopic, throttleDelay);
	_pubStateInterval = pubStateInterval;
}

void SerialRelayGroup::subscribe(PubSubClient& client)
{
	char buf[50];
	strcpy(buf, _topic);
	strcat(buf, "#");
	client.subscribe(buf);	// subscribe only once to higher level parent topic and filter at a relay level the exact relay
}

bool SerialRelayGroup::updateState(char * topic, unsigned char * payload, unsigned int length, PubSubClient & client)
{
	int i = 0;
	bool updated = false;
	while (i < _actuatorCount) {
		if (_relays[i++]->Actuator::updateState(topic, payload, length, client))
			updated = true;
	}
	if (_mutuallyExclusive && updated)
		pollState(client, true);	// forced pol state for mutually exclusive
	return true;
}

bool SerialRelayGroup::updateState(uint pin, uint value)
{
	byte newState = 0x01 << pin;
	if (_mutuallyExclusive)
		_state = 0x00;	// all OFF
	_state = (value == HIGH) ? _state | newState : _state & ~newState;
	//	PrintHex8(&_state, 1);
	byte physicalState = _onState == HIGH ? _state : ~_state;
#ifdef LOGGING_ON
	Station::log("Updating virtual pin (Relay Group - overridden)...");
	PrintHex8(&newState, 1);
	PrintHex8(&_state, 1);
	PrintHex8(&physicalState, 1);
#else
	Serial.write(physicalState);
	delay(_throttleDelay);
#endif // !LOGGING_ON
	return true;
}

void SerialRelayGroup::init(int pinCount, int onState, int initialState, const char * topic, const char * stateTopic, long throttleDelay)
{
	Station::log("Relay group init ...");
	_pin = pinCount;
	_onState = onState;
	strcpy(_topic, topic);
	strcpy(_stateTopic, stateTopic);
	_throttleDelay = throttleDelay != 0 ? throttleDelay : 200;	// 5 times/sec default minimum
	_state = initialState == LOW ? 0x00 : 0xFF;	// set current state as opposite to desired initial so the first update would pass.
#ifndef LOGGING_ON
	Serial.write(_initSequense[0]);
	delay(500);
	Serial.write(_initSequense[1]);
	delay(500);
	Serial.write(_onState != initialState ? 0x00 : 0xFF);
	delay(500);
#endif // !LOGGING_ON

	//	Serial.write(0x00);
	char topicPrefix[50];
	char stateTopicPrefix[50];
	char topicSuffix[10];
	while (_actuatorCount < pinCount) {
		sprintf(topicSuffix, "%02i", _actuatorCount);

		strcpy(topicPrefix, topic);
		strcat(topicPrefix, topicSuffix);

		strcpy(stateTopicPrefix, stateTopic);
		strcat(stateTopicPrefix, topicSuffix);
		_relays[_actuatorCount] = new SerialRelay(_actuatorCount, _onState, initialState,
			topicPrefix, stateTopicPrefix, _throttleDelay, this);
		_actuatorCount++;
	}
}

void SerialRelayGroup::pollState(PubSubClient & client, bool forced)
{
	int i = 0;
	while (i < _actuatorCount)
		_relays[i++]->Actuator::pollState(client, forced);
}
