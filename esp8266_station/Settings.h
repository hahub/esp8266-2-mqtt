#ifndef Settings_h
#define Settings_h
// you need your own WiFi.h with your own settings or uncomment the 2 following lines
#include "WiFi.h"
const char* wifi_ssid = MYSSID;
const char* wifi_password = MYPASSWORD;
const char* mqtt_ip = MYMQTTIP;
const int mqtt_port = 1883;
const char* mqtt_client = "Station07 - HVAC v0.2";	// must be different for different stations
const char* mqtt_anouncement_topic = "/global";	// updated when station is up.

// constants only for sensors that you use. others just ignore.
//const char* mqtt_motion_topic = "/house/hvac/motion"; //pin=4 (D2), on=1, 200ms
const char* mqtt_hvac_fan_state_topic = "/house/hvac/pressure/state"; //pin=5 (D1), on=1, 1000ms
const char* mqtt_hvac_air_temperature_topic = "/house/hvac/out/temperature"; //pin=14 (D5), 1000ms
//const char* mqtt_motion_topic = "/house/office/motion"; //pin=13 (D7), on=1, 200ms
//const char* mqtt_irrigation_base_topic = "/lawn/irrigation/command/";	// commands will go to subtopic like /lawn/irrigation/command/01
//const char* mqtt_irrigation_state_base_topic = "/lawn/irrigation/state/"; //states will come from subtopics like /lawn/irrigation/state/01
const char* mqtt_temperature_topic = "/house/workshop/temperature";
const char* mqtt_humidity_topic = "/house/workshop/humidity";
#endif
