#ifndef Sensor_h
#define Sensor_h

#include <PubSubClient.h>

class Sensor
{
public:
	Sensor(int pin, int onState, int pullUp, const char* mqttTopic, long throttleDelay);
	Sensor();
	void refresh(PubSubClient& client);
protected:
	virtual float getValue();
	int getValue(int pin);
	void refreshDigital(PubSubClient& client);
	float cleanupNoise(float newValue);
	void refreshByCb(PubSubClient& client);
	int _pin;
	int _onState;
	const char* _mqttTopic;
	long _throttleDelay;
	long _lastPoll;
	int _state;
	float _stateDecimal;
#define AVGSIZE 20
	float _lastValues[AVGSIZE] = { 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 
								   0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00 };
	uint _nextLastIdx = 0;
	bool _binary;
	bool _reportIfUnchanged;
};
#endif
