#include "IrrigationRelays.h"

IrrigationRelays::IrrigationRelays(int pin, int onState, int initialState, const char* topic, const char* stateTopic, long throttleDelay)
{
	_mutuallyExclusive = true;
	init(pin, onState, initialState, topic, stateTopic, throttleDelay);	
}
