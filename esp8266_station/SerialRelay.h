// SerialRelay.h

#ifndef _SERIALRELAY_h
#define _SERIALRELAY_h
#include "Actuator.h"
//#include "SerialRelayGroup.h"

class SerialRelay : public Actuator
{
public:
	SerialRelay(int pin, int onState, int initialState, const char * topic, const char* stateTopic, long throttleDelay, Actuator * relayGroup);
	bool updateState(uint pin, uint value);
protected:
	void init(int pin, int onState, int initialState, const char* topic, const char* stateTopic, long throttleDelay, Actuator * relayGroup);
	Actuator * _relayGroup = NULL;	//  to update group state
};

#endif