#ifndef DTSensor_h
#define DTSensor_h
#include "Sensor.h"
#include <OneWire.h>
#include <DallasTemperature.h>

class DTSensor : public Sensor
{
public:
	DTSensor(int _pin, int pullUp, const char* mqttTopic, long throttleDelay);
protected:
	virtual float getValue();
private:
	DallasTemperature* _dt;
};
#endif
