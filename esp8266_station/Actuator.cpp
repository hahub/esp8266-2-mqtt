﻿#include "Flags.h"
#include "Station.h"
#include "Actuator.h"

Actuator::Actuator()
{
}

Actuator::Actuator(int pin, int onState, int initialState, const char * topic, const char* stateTopic, long throttleDelay)
{
	init(pin, onState, initialState, topic, stateTopic, throttleDelay);
}

void Actuator::subscribe(PubSubClient& client)
{
	client.subscribe(_topic);
	Station::log("Subscribed to topic: ");
	Station::log(_topic);
}

bool Actuator::updateState(char * topic, unsigned char * payload, unsigned int length, PubSubClient& client)
{
	bool updated = false;
	if (strcmp(topic, _topic) == 0)
	{
		Station::log("Updating Actuator for matching topic: ");	Station::log(topic); Station::log(_topic);
		updated = updateState((char)payload[0] == 'O' && (char)payload[1] == 'N' ?
			HIGH : LOW);	// ON/OFF - logical states!!
		if (!_mutuallyExclusive)	// otherwise parent will publish all states
			publishState(client);
	}
	else if (_mutuallyExclusive && _state == HIGH) {	// use when working in group. Only one can stay ON. and it is not me...
		updated = overrideState(LOW, client);
	}
	return		updated;
}

void Actuator::init(int pin, int onState, int initialState, const char * topic, const char * stateTopic, long throttleDelay)
{
	_pin = pin;
	_onState = onState;
	strcpy(_topic, topic);
	strcpy(_stateTopic, stateTopic);
	_throttleDelay = throttleDelay != 0 ? throttleDelay : 500;	// 2 times/sec default minimum
	_state = initialState; // set current state as opposite to desired initial so the first update would pass.
	pinMode(_pin, OUTPUT);
	updateState(_pin, initialState);
}

bool Actuator::updateState(uint8_t newState)
{
	long now = millis();
	if (now - _lastUpdate > _throttleDelay) {
		_lastUpdate = now;
		if (newState != _state)
		{
			_state = newState;
			return this->updateState(_pin, _state);
		}
	}
	return false;
}

bool Actuator::overrideState(uint8_t newState, PubSubClient & client)
{
	bool updated = _state != newState;
	_state = newState;
//	_lastUpdate = millis();
	//	this->updateState(_pin, _state);
	//	this->publishState(client);
	return updated;
}

bool Actuator::updateState(uint pin, uint state)
{
	uint physicalState = _onState != state ? LOW : HIGH;
	digitalWrite(pin, physicalState);
	return true;
}

void Actuator::publishState(PubSubClient & client)
{
	if (_stateTopic != NULL)
		client.publish(_stateTopic, _state == HIGH ? "ON" : "OFF");
}

void Actuator::pollState(PubSubClient & client, bool forced)
{
	long now = millis();
	if (now - _lastStatePublished > _pubStateInterval || forced) {
		_lastStatePublished = now;
		publishState(client);
	}
}