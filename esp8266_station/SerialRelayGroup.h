// SerialRelayGroup.h

#ifndef _SERIALRELAYGROUP_h
#define _SERIALRELAYGROUP_h
#include "SerialRelay.h"

class SerialRelayGroup : public Actuator
{
public:
	SerialRelayGroup();
	SerialRelayGroup(int pin, int onState, int initialState, const char* topic, const char* stateTopic, long throttleDelay);
	SerialRelayGroup(int pin, int onState, int initialState, const char * topic, const char * stateTopic, long throttleDelay, long pubStateInterval);
	virtual bool updateState(char * topic, unsigned char * payload, unsigned int length, PubSubClient& client);
	virtual bool updateState(uint pin, uint value);
	virtual void pollState(PubSubClient & client, bool forced = false);
	void subscribe(PubSubClient& client);
protected:
	void init(int pinCount, int onState, int initialState, const char* topic, const char* stateTopic, long throttleDelay);
	byte _state;	// represents group state byte to push to UART
	const byte _initSequense[2] = { 0x50, 0x51 };	// for icstation.com usb/uart relay boards
	SerialRelay* _relays[10];
	int _actuatorCount = 0;
};
#endif

