#include <DHT.h>
#include "Flags.h"
#include "Settings.h"
#include "SerialRelay.h"
#include "SerialRelayGroup.h"
#include "IrrigationRelays.h"
#include "Station.h"
#include "DTSensor.h"
#include "DHT11Sensor.h"

Station station(wifi_ssid, wifi_password,
	mqtt_ip, mqtt_port, mqtt_client, mqtt_anouncement_topic);
DHT * dht1 = NULL;

void setup()
{
#ifdef LOGGING_ON
	Serial.begin(115200);
	delay(500);
#else	// either use logging to COM or use COM to control serial relay group
	Serial.begin(9600);
	delay(500);
#endif
	//// Important: ESP8266 pins have not the same numbers as Arduino
	//// in all examples using ON and OFF as message body in mqtt. consumable by OpenHAB
	//// Simple motion (any ON/OFF) sensor example. on pin D7 (esp8266). PIR sensor HC-SR501
	//// 1 = ON. Do not pull up - sensor provides levels. 200ms throttled (sensor has its own delay adjustments)
	//station.registerSensor(new Sensor(5, 1, 1, mqtt_motion_topic, 200));

	//// add DHT11 to your station on pin 12. both sensors use the same DHT object instance (the first called creates it)
	//// current implementation has "noise cancellation" which increases precision and hides spikes.
	station.registerSensor(new DHT11Sensor(TEMPERATURE, 12, dht1, mqtt_temperature_topic, 30 * SECONDS));
	station.registerSensor(new DHT11Sensor(HUMIDITY, 12, dht1, mqtt_humidity_topic, 30 * SECONDS));

	//// HVAC vacuum sensor is very important so we need to pass its state immediately (cannot miss it!). 100ms throttle. 
	//// 0 = ON (it shortcuts to the ground when air blows). Pulled up to have real 1 (Vcc) on it when idle.
	station.registerSensor(new Sensor(5, 0, 1, mqtt_hvac_fan_state_topic, 100));

	//// Temperature changes too often and it's enough to poll it to once per 10 sec (or even more). Sensor: DS18B20. No need for resitor if Pull-up the output.
	station.registerSensor(new DTSensor(14, 1, mqtt_hvac_air_temperature_topic, 10 * SECONDS));

	//// a relay listening on D0(esp8266) pin. LOW - active. Initialized to HIGH (OFF). Should work for any relay module with binary inputs, sometimes with optocouplers.
	//// will not let change it more often then 1 sec. will send its status update on mqtt when command succeeded.
	//station.registerActuator(new Actuator(16, LOW, HIGH, "/house/test2", "/house/test2feedback", 1 * SECONDS));

	//// a 8-relay RelayGroup www.icstation.com connected on Serial line (only one is supported). 
	//// Physical LOW (0) is active = ON. Initialized to logical LOW = OFF
	//station.registerActuator(new IrrigationRelays(8, LOW, LOW, mqtt_irrigation_base_topic, mqtt_irrigation_state_base_topic, 200));
};

void loop()
{
	station.refresh();
};

