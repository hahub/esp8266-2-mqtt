#ifndef Station_h
#define Station_h

#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include "Sensor.h"
#include "Actuator.h"

#define SECONDS 1000

class Station
{
public:
	Station(const char* wifiSSID, const char* wifiPassword,
		const char* mqttIP, int mqttPort, const char* mqttClient,
		const char* anouncementTopic);
	void refresh();
	void refreshSensors();
	void registerSensor(Sensor* sensor);
	void updateActuatorsFromMqtt(char * topic, unsigned char * payload, unsigned int length);
	void registerActuator(Actuator* actuator);
	static void log(const char*);
	static void log(double val);
private:
	int _pin;
	const char* _wifiSSID;
	const char* _wifiPassword;
	const char* _mqttIP;
	int _mqttPort;
	const char* _mqttClient;
	const char* _anouncementTopic;
	WiFiClient _espClient;
	PubSubClient _pubSubClient;
	void connectWiFi();
	int _sensorCount=0;
	int _currentSensor = 0; // loop will refresh one sensor at a time
	int _actuatorCount;
	Sensor* _sensors[10];
	Actuator* _actuators[10];
	void subscribeActuators();
	void pollActuatorsState();
	int _step = 1;	// each loop we'll do one action ( connect-1, read sensors-2 or update pubsub-3)
};

#endif
