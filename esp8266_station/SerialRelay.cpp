#include "Flags.h"
#include "SerialRelay.h"
#include "Station.h"

SerialRelay::SerialRelay(int pin, int onState, int initialState, const char * topic, const char * stateTopic, long throttleDelay, Actuator * relayGroup)
{
	init(pin, onState, initialState, topic, stateTopic, throttleDelay, relayGroup);
}

void SerialRelay::init(int pin, int onState, int initialState, const char * topic, const char * stateTopic, long throttleDelay, Actuator * relayGroup)
{
	Station::log("Serial Relay init ...");
	Station::log(topic);
	Station::log(stateTopic);
	_pin = pin;
	_onState = onState;
	strcpy(_topic, topic);
	strcpy(_stateTopic, stateTopic);
	_throttleDelay = throttleDelay;
	_state = initialState;
	_relayGroup = relayGroup;	// for now serial relay is only working in groups.
	_mutuallyExclusive = relayGroup->isMutuallyExclusive();
}

bool SerialRelay::updateState(uint pin, uint value)
{
	Station::log("Updating pin (Relay)...");
	return _relayGroup->updateState(pin, value);
}