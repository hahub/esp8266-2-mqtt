#pragma once
#include "SerialRelayGroup.h"
class IrrigationRelays :
	public SerialRelayGroup
{
public:
	IrrigationRelays(int pin, int onState, int initialState, const char* topic, const char* stateTopic, long throttleDelay);
};

