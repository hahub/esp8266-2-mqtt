#include "Sensor.h"

Sensor::Sensor(int pin, int onState, int pullUp, const char* mqttTopic, long throttleDelay)
{
	_reportIfUnchanged = false;	// only report sensor if changed
	_binary = true;
	_pin = pin;
	_onState = onState;
	_state = onState == HIGH ? LOW : HIGH;
	_lastPoll = millis();
	_mqttTopic = mqttTopic;
	_throttleDelay = throttleDelay;

	pinMode(_pin, pullUp ? INPUT_PULLUP : INPUT);
}

Sensor::Sensor()
{}

void Sensor::refresh(PubSubClient& client)
{
	if (_binary)
		refreshDigital(client);
	else
		refreshByCb(client);
}

float Sensor::getValue()
{
	return 42.42;	// yes, this is the answer to the main question of the universe..
}

int Sensor::getValue(int pin)
{
	return digitalRead(pin);
}

void Sensor::refreshByCb(PubSubClient& client)
{
	long now = millis();
	if (now - _lastPoll > _throttleDelay) {
		double newState = getValue();
		_lastPoll = now;
		if (newState != _stateDecimal || _reportIfUnchanged)
		{
			_stateDecimal = newState;
			char buf[10];
			dtostrf((double)_stateDecimal, 4, 2, buf);
			client.publish(_mqttTopic, buf);
		}
	}
}

void Sensor::refreshDigital(PubSubClient& client)
{
	long now = millis();
	if (now - _lastPoll > _throttleDelay) {
		int newState = getValue(_pin);
		_lastPoll = now;
		if (newState != _state)
		{
			_state = newState;
			client.publish(_mqttTopic, _state == _onState ? "ON" : "OFF");
		}
	}
}

// Noise Cleanup would always output average of the past 10-20 measurements. 
// So all the noise spikes will disappear but will still contribute to the overall picture. 
// ToDo: add more logic. e.g. ignore really quick and unrealistic spikes, MIN and MAX values
// that come up as part of noise.
float Sensor::cleanupNoise(float newValue)
{
	if (_nextLastIdx > AVGSIZE)
		_nextLastIdx = 0;
	if (!isnan(newValue))
		_lastValues[_nextLastIdx++] = newValue;

	int count = 0;
	float sum = 0.00;
	for (int i = 0; i < AVGSIZE; i++)
	{
		float current = _lastValues[i];
		if (current != 0.00)
		{
			sum += current;
			count++;
		}
	}

	if (count == 0)
		return newValue;

	return sum / count;
}

