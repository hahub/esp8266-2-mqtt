#include "DTSensor.h"

DTSensor::DTSensor(int _pin, int pullUp, const char* mqttTopic, long throttleDelay)
{
	OneWire* oneWire = new OneWire(_pin);
	// if you do not have external resistor - force PullUp=1 for internal resistor use. 
	// it is not documented well so use at own risk (may not work in all cases)
	if (pullUp)	
		pinMode(_pin, INPUT_PULLUP);
	_dt = new DallasTemperature(oneWire);

	_binary = false;
	_mqttTopic = mqttTopic;
	_throttleDelay = throttleDelay;
}

float DTSensor::getValue()
{
	_dt->requestTemperatures();
	return _dt->getTempCByIndex(0);
}


