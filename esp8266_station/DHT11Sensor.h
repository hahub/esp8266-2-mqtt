#pragma once
#include "Sensor.h"
#include <DHT.h>
#define DHTTYPE DHT11
#define TEMPERATURE 1
#define HUMIDITY 2

class DHT11Sensor :
	public Sensor
{
public:
	DHT11Sensor(int type, int pin, const char* mqttTopic, long throttleDelay);
	DHT11Sensor(int type, int pin, DHT * indht, const char* mqttTopic, long throttleDelay);
	DHT * _dht = NULL;
protected:
	virtual float getValue();
	bool _isTemperature;
};

